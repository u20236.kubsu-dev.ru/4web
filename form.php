<?php

header("Content-Type: text/html; charset=windows-1251");

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    
    
    if (!empty($_COOKIE['save'])) {
        
        setcookie('save', '', 100000);
        
        echo '<div class="text-center text-white w-50 mx-auto my-1">Results saved</div>';
    }
    
    
    $errors = array();
    $errors['Name'] = !empty($_COOKIE['Name_error']);
    $errors['EMail'] = !empty($_COOKIE['EMail_error']);
    $errors['Year'] = !empty($_COOKIE['Year_error']);
    $errors['Biog'] = !empty($_COOKIE['Biog_error']);
    $errors['Sex'] = !empty($_COOKIE['Sex_error']);
    $errors['KolKon'] = !empty($_COOKIE['KolKon_error']);
    $errors['Checkbox'] = !empty($_COOKIE['Checkbox_error']);
    $errors['Sposobnosti'] = !empty($_COOKIE['Sposobnosti_error']);
    
    if ($errors['Name'] && $_COOKIE['Name_error'] == 1) {
        setcookie('Name_error', '', 100000);
        echo '<div class="error text-center text-white w-50 mx-auto my-1">Input Name</div>';
    }
    if ($errors['Name'] && $_COOKIE['Name_error'] == 2) {
        setcookie('Name_error', '', 100000);
        echo '<div class="error text-center text-white w-50 mx-auto my-1">Input Correct Name</div>';
    }
    if ($errors['EMail'] && $_COOKIE['EMail_error']== 1) {
        setcookie('EMail_error', '', 100000);
        echo '<div class="error text-center text-white w-50 mx-auto my-1">Input E-mail</div>';
    }
    if ($errors['EMail'] && $_COOKIE['EMail_error']== 2) {
        setcookie('EMail_error', '', 100000);
        echo '<div class="error text-center text-white w-50 mx-auto my-1">Input Correct E-mail</div>';
    }
    if ($errors['Year'] && $_COOKIE['EMail_error']== 1) {
        setcookie('Year_error', '', 100000);
        echo '<div class="error text-center text-white w-50 mx-auto my-1">Input Year</div>';
    }
    if ($errors['Year'] && $_COOKIE['EMail_error']== 2) {
        setcookie('Year_error', '', 100000);
        echo '<div class="error text-center text-white w-50 mx-auto my-1">Input Correct Year</div>';
    }
    if ($errors['Biog']) {
        setcookie('Biog_error', '', 100000);
        echo '<div class="error text-center text-white w-50 mx-auto my-1">Input Boifgrafy</div>';
    }
    if ($errors['Sex']) {
        setcookie('Sex_error', '', 100000);
        echo '<div class="error text-center text-white w-50 mx-auto my-1">Select Sex</div>';
    }
    if ($errors['KolKon']) {
        setcookie('KolKon_error', '', 100000);
        echo '<div class="error text-center text-white w-50 mx-auto my-1">Select Kolichestvo konechnostey</div>';
    }
    if ($errors['Checkbox']) {
        setcookie('Checkbox_error', '', 100000);
        echo '<div class="error text-center text-white w-50 mx-auto my-1">Agree to the instructions</div>';
    }
    if ($errors['Sposobnosti']) {
        setcookie('Sposobnosti_error', '', 100000);
        echo '<div class="error text-center text-white w-50 mx-auto my-1">Select Sposobnosti</div>';
    }
    
    $values = array();
    $values['Name'] = empty($_COOKIE['Name_value']) ? '' : $_COOKIE['Name_value'];
    $values['EMail'] = empty($_COOKIE['EMail_value']) ? '' : $_COOKIE['EMail_value'];
    $values['Year'] = empty($_COOKIE['Year_value']) ? '' : $_COOKIE['Year_value'];
    $values['Biog'] = empty($_COOKIE['Biog_value']) ? '' : $_COOKIE['Biog_value'];
    $values['Sex'] = empty($_COOKIE['Sex_value']) ? '' : $_COOKIE['Sex_value'];
    $values['KolKon'] = empty($_COOKIE['KolKon_value']) ? '' : $_COOKIE['KolKon_value'];
    $values['Checkbox'] = empty($_COOKIE['Checkbox_value']) ? '' : $_COOKIE['Checkbox_value'];
    $values['Sposobnosti'] = array();
    if (key_exists("Sposobnosti_value",$_COOKIE)) 
        $values['Sposobnosti'] = unserialize($_COOKIE['Sposobnosti_value'], 
            ["allowed_classes" => false]);
    
    include('form.php');
}
else {
    
    $errors = FALSE;
    if (empty($_POST['Name'])) {
        
        setcookie('Name_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else if (!ctype_alpha($_POST['Name'])) {
        setcookie('Name_error', '2', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('Name_value', $_POST['Name'], time() + 30 * 24 * 60 * 60);
    }
    
    
    if (empty($_POST['EMail'])) {
        setcookie('EMail_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else if (!preg_match("/^[a-zA-Z0-9_\-.]+@[a-z]/", $_POST['Email'])) {
        setcookie('EMail_error', '2', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('EMail_value', $_POST['EMail'], time() + 30 * 24 * 60 * 60);
    }
    
    
    if (empty($_POST['Year'])) {
        setcookie('Year_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else if (!ctype_digit($_POST['Year'])) {
        setcookie('Year_error', '2', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('Year_value', $_POST['Year'], time() + 30 * 24 * 60 * 60);
    }
    
    
    if (empty($_POST['Biog'])) {
        setcookie('Biog_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('Biog_value', $_POST['Biog'], time() + 30 * 24 * 60 * 60);
    }
    
    
    if (empty($_POST['Sex'])) {
        setcookie('Sex_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('Sex_value', $_POST['Sex'], time() + 30 * 24 * 60 * 60);
    }
    
    
    if (empty($_POST['KolKon'])) {
        setcookie('KolKon_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('KolKon_value', $_POST['KolKon'], time() + 30 * 24 * 60 * 60);
    }
    
    if (empty($_POST['Checkbox']) && key_exists("Checkbox_value",$_COOKIE)) {
        setcookie('Checkbox_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        setcookie('Checkbox_value', $_POST['Checkbox'], time() + 30 * 24 * 60 * 60);
    }
    
    
    if (count($_POST['Sposobnosti'])==0) {
        $spos = array();
        setcookie('Sposobnosti_value', serialize($spos), time() + 30 * 24 * 60 * 60);
    } else {
        setcookie('Sposobnosti_value', serialize($_POST['Sposobnosti']), time() + 30 * 24 * 60 * 60);
    }
    if ($errors) {
        header('Location: index.php');
        exit();
    }
    else {
        setcookie('Name_error', '', 100000);
        setcookie('EMail_error', '', 100000);
        setcookie('Year_error', '', 100000);
        setcookie('Biog_error', '', 100000);
        setcookie('Sex_error', '', 100000);
        setcookie('KolKon_error', '', 100000);
        setcookie('Checkbox_error', '', 100000);
        setcookie('Sposobnosti_error', '', 100000);
    }
    
    $sposobnosti='';
    
    for($i=0;$i<count($_POST['Sposobnosti']);$i++){
        $sposobnosti .= $_POST['Sposobnosti'][$i] . ' ';
    }
    
    $user = 'u20236';
    $pass = '8398991';
    $db = new PDO('mysql:host=localhost;dbname=u20236', $user, $pass, 
        array(PDO::ATTR_PERSISTENT => true));
    
    try {
        $stmt = $db->prepare("INSERT INTO form (Name,Year,EMail,Sex,KolKon,Sposobnosti,
Biog,Checkbox) VALUES (:Name,:Year,:EMail,:Sex,:KolKon,:Sposobnosti,:Biog,:Checkbox)");
        $stmt -> execute(array('Name'=>$_POST['Name'],'Year'=>$_POST['Year'],
            'EMail'=>$_POST['EMail'],'Sex'=>$_POST['Sex'],'KolKon'=>$_POST['KolKon'],
            'Sposobnosti'=>$sposobnosti, 'Biog'=>$_POST['Biog'],'Checkbox'=>$ch));
    }
    catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
    }
    
    setcookie('save', '1');
    
    header('Location: index.php');
}
?>
